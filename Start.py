import pygame, time, random, os, sys, math
from Settings import *
from Obstacles import *
from Bullet import *
from Character import *
from pygame.locals import *

pygame.init()

#PROSTOKĄTY OZNACZAJACE PASKI HP
def player1HPrect(hp_x, hp_y, hp_width, hp_height, color):
 pygame.draw.rect(gameDisplay, color, [hp_x, hp_y, hp_width, hp_height])
 
def player2HPrect(hp2_x, hp2_y, hp2_width, hp2_height, color):
 pygame.draw.rect(gameDisplay, color, [hp2_x, hp2_y, hp2_width, hp2_height])

#LICZNIK HP 
def player1HP(count):
 font = pygame.font.SysFont(None, 45)
 text = font.render("HP: " + str(count), True, white)
 gameDisplay.blit(text,(50,50))
 
def player2HP(count):
 font = pygame.font.SysFont(None, 45)
 text = font.render("HP: " + str(count), True, white)
 gameDisplay.blit(text,(660,50)) 

#LICZNIK ZWYCIESTW
def player1WINS(count):
 font = pygame.font.SysFont(None, 45)
 text = font.render("WINS: " + str(count), True, white)
 gameDisplay.blit(text,(50,20)) 

def player2WINS(count):
 font = pygame.font.SysFont(None, 45)
 text = font.render("WINS: " + str(count), True, white)
 gameDisplay.blit(text,(660,20))  

#DEFINICJE TEKSTU
def text_objects(text, font):
 textSurface = font.render(text, True, black)
 return textSurface, textSurface.get_rect()

def round1_text():
 font = pygame.font.SysFont(None, 75)
 text = font.render("ROUND 1", True, black)
 gameDisplay.blit(text,(display_width * 0.35 ,20 ))
 
def round2_text():
 font = pygame.font.SysFont(None, 75)
 text = font.render("ROUND 2", True, black)
 gameDisplay.blit(text,(display_width * 0.35 ,20 ))

def round3_text():
 font = pygame.font.SysFont(None, 75)
 text = font.render("ROUND 3", True, black)
 gameDisplay.blit(text,(display_width * 0.35 ,20 ))

def winner_message(text):
 largeText = pygame.font.Font('freesansbold.ttf',100)
 TextSurf, TextRect = text_objects(text,largeText)
 TextRect.center = ((display_width/2),(display_height/2))
 gameDisplay.blit(TextSurf, TextRect)
 
 pygame.display.update()
 time.sleep(5)
 START.game_menu()

def winner_1():
 winner_message('Wygral PI')

def winner_2():
 winner_message('Wygral PII')
 
#DEFINICJA BUTTONÓW OBSŁUGIWANYCH MYSZKĄ
def button(msg, x, y, w, h, inact, act, action = None):
 mouse = pygame.mouse.get_pos()
 click = pygame.mouse.get_pressed()

 if x + w > mouse[0] > x and  y + h > mouse[1] > y:
  pygame.draw.rect(gameDisplay, act, (x, y, w, h))
  if click[0] == 1 and action != None:
   if action == "play":
    SCENE.game_loop()
   elif action == "help":
    HELP.help_screen()
   elif action == "quit":
    pygame.quit()
   elif action == "back":
    INTRO.game_menu()
 
 else:
  pygame.draw.rect(gameDisplay, inact, (x, y, w, h))
   
 smallText = pygame.font.Font("freesansbold.ttf", 32)
 textSurf, textRect = text_objects(msg, smallText)  
 textRect.center = ( (x + (w/2)), (y + (h/2)))
 gameDisplay.blit(textSurf, textRect)
 
class START:
 
 def game_menu():#klasa menu
  pygame.mixer.music.stop
  pygame.mixer.music.load('sounds/Menu.mp3')
  pygame.mixer.music.play(-1,0.0)
  
  menu = True
 
  while menu:
   for event in pygame.event.get():
    if event.type == pygame.QUIT:
     quit()
   gameDisplay.blit(menu_image, [0,0])

   button("START", 40, 124, 350, 40, button_1Off, button_1, "play")
   button("HELP", 40, 180, 350, 40, button_2Off, button_2, "help")
   button("EXIT", 40, 236, 350, 40, button_3Off, button_3, "quit")
   pygame.display.update()
   clock.tick(15)

class HELP:
 #pisane po angielsku o 24:22, tak w razie czego
 def help_screen():#klasa menu
  pygame.mixer.music.stop
  pygame.mixer.music.load('sounds/Menu.mp3')
  pygame.mixer.music.play(-1,0.0)
 
  help = True
 
  while help:
   for event in pygame.event.get():
    if event.type == pygame.QUIT:
     quit()
   gameDisplay.blit(help_image, [0,0])

   button("START", 40, 240, 350, 40, button_1Off, button_1, "play")
   button("EXIT", 40, 360, 350, 40, button_3Off, button_3, "quit")
   pygame.display.update()
   clock.tick(29)
   
class SCENE:
 
 def ground(ground_x, ground_y, ground_width, ground_height):
  gameDisplay.blit(ground_image, (ground_x, ground_y, ground_width, ground_height))
 
 def ground2(ground_x, ground_y, ground_width, ground_height):
  gameDisplay.blit(ground_image2, (ground_x, ground_y, ground_width, ground_height))
  
 def ground3(ground_x, ground_y, ground_width, ground_height):
  gameDisplay.blit(ground_image3, (ground_x, ground_y, ground_width, ground_height))

 def game_loop():
  x_start_Char = (100) #poczatkowe ustawienie gracza x
  y_start_Char = (245) #poczatkowe ustawienie gracza y
  x_start_Char2 = (600)
  y_start_Char2 = (237)
  char_w= 45
  char_h= 145
  char2_w= 45
  char2_h= 145
  player1_HP = 250
  player2_HP = 250
  player1_WINS = 0
  player2_WINS = 0
  obs_x = random.randrange(0, display_width) 
  obs_y = -1000
  obs_speed = 10
  obs_speed2 = 13
  obs_width = 40
  obs_height = 40
  obs2_x = random.randrange(0, display_width) 
  obs2_y = -1000
  obs2_speed = 10
  obs2_width = 40
  obs2_height = 40
  bullet1w = 40
  bullet2w = 40
  bullet1h = 40
  bullet2h = 40
  bullet1x = random.randrange(450, display_width-50)
  bullet1y = 0
  bullet2x = random.randrange(0, 300)
  bullet2y = 0
  
  pygame.mixer.music.load('sounds/Game.mp3')
  pygame.mixer.music.play(-1,0.0)

  x_CharChange = 0
  y_CharChange = 0
  x_CharChange2 = 0
  y_CharChange2 = 0
  #x_BulletChange = 0
  #x_BulletChange2 = 0
  y_BulletChange = 0
  y_BulletChange2 = 0
 
  gameExit = False

  while not gameExit:
    
   for event in pygame.event.get():
    if event.type == pygame.QUIT:
     gameExit = True and quit()#tutaj wazne
   
    if event.type == pygame.KEYDOWN:
     if event.key == pygame.K_w:
      y_CharChange = -30
     elif event.key == pygame.K_a:
      x_CharChange = -15
     elif event.key == pygame.K_d:
      x_CharChange = 15	
     elif event.key == pygame.K_SPACE:
      y_BulletChange = 25
     elif event.key == pygame.K_k:
      y_BulletChange2 = 25
	
    if event.type == pygame.KEYUP:
      if event.key == pygame.K_a:
       x_CharChange = 0
      elif event.key == pygame.K_d:
       x_CharChange = 0

    if event.type == pygame.KEYDOWN:
     if event.key == pygame.K_UP:
      y_CharChange2 = -30
     elif event.key == pygame.K_RIGHT:
      x_CharChange2 = 15
     elif event.key == pygame.K_LEFT:
      x_CharChange2 = -15
	  
    if event.type == pygame.KEYUP:
     if event.key == pygame.K_LEFT:
      x_CharChange2 = 0
     elif event.key == pygame.K_RIGHT:
      x_CharChange2 = 0

#SKOK FIZYKA	
   if y_start_Char < 150:
    y_CharChange = 40
   if y_start_Char > 245:
    y_CharChange = 0
    y_start_Char = 245
   
   if y_start_Char2 < 125:
    y_CharChange2 = 40
   if y_start_Char2 > 237:
    y_CharChange2 = 0
    y_start_Char2 = 237
  
   if x_start_Char > 300:
    x_start_Char = 300
   if x_start_Char < 0:
    x_start_Char = 0
   if x_start_Char2 < 400:
    x_start_Char2 = 400
   if x_start_Char2 > 650:
    x_start_Char2 = 650

#REGUŁKA NA TWORZENIE NOWEGO/TEGO SAMEGO POCISKU
   if bullet1y > display_height:
    bullet1y = 0
    bullet1x = random.randrange(400,display_width)
    y_BulletChange = 0
   if bullet2y == display_height:
    bullet2y = 0
    bullet2x = random.randrange(0,300)
    y_BulletChange2 = 0
   if obs_y > display_height:
    obs_y = 0 - obs_height
    obs_x = random.randrange(0,display_width)
   if obs2_y > display_height:
    obs2_y = 0
    obs2_x = random.randrange(0,display_width)
	
#ZMIANA PREDKOSCI/RUCH POCISKOW/RUCH POSTACI	
   bullet1y += y_BulletChange
   bullet2y += y_BulletChange2
   x_start_Char += x_CharChange
   y_start_Char += y_CharChange
   x_start_Char2 += x_CharChange2
   y_start_Char2 += y_CharChange2
  
   obs_y += obs_speed
   obs2_y += obs_speed2
  
#KOLIZJA/UTRATAHP	
   if y_start_Char < obs_y + obs_height:
    player1_HP -= 0
   
    if x_start_Char >  obs_x and x_start_Char < obs_x + obs_width or x_start_Char + char_w > obs_x and x_start_Char + char_w < obs_x + obs_width:
     player1_HP -= 10
     obs_y = 0
     obs_x = random.randrange(0,display_width)

   if y_start_Char2 < obs_y + obs_height:
    player2_HP -= 0
   
    if x_start_Char2 >  obs_x and x_start_Char2 < obs_x + obs_width or x_start_Char2 + char_w > obs_x and x_start_Char2 + char_w < obs_x + obs_width:
     player2_HP -= 10
     obs_y = 0
     obs_x = random.randrange(0,display_width)
     
   
   if y_start_Char2 < obs2_y + obs2_height:
    player2_HP -= 0
   
    if x_start_Char2 >  obs2_x and x_start_Char2 < obs2_x + obs2_width or x_start_Char2 + char_w > obs2_x and x_start_Char2 + char_w < obs2_x + obs2_width:
     player2_HP -= 10
     obs2_y = 0
     obs2_x = random.randrange(0,display_width)
      
   if y_start_Char < obs2_y + obs2_height:
    player1_HP -= 0
   
    if x_start_Char >  obs2_x and x_start_Char < obs2_x + obs2_width or x_start_Char + char_w > obs2_x and x_start_Char + char_w < obs2_x + obs2_width:
     player1_HP -= 10
     obs2_y = 0
     obs2_x = random.randrange(0,display_width)

   if player1_HP == 0:
    player2_WINS += 1
    player1_HP = 250
    player2_HP = 250
	
   if player2_HP == 0:
    player1_WINS += 1
    player2_HP = 250
    player1_HP = 250
	
#KOLIZJA Z POCISKAMI
   if y_start_Char2 < bullet1y + bullet1h:
    player2_HP -= 0
    
    if x_start_Char2 >  bullet1x and x_start_Char2 < bullet1x + bullet1h or x_start_Char2 + char_w > bullet1x and x_start_Char2 + char_w < bullet1x + bullet1w:
     player2_HP -= 10
     bullet1y = 0
     bullet1x = random.randrange(400,display_width)
     y_BulletChange = 0
     
   if y_start_Char < bullet2y + bullet2h:
    player1_HP -= 0
    
    if x_start_Char >  bullet2x and x_start_Char < bullet2x + bullet2h or x_start_Char + char_w > bullet2x  and x_start_Char + char_w < bullet2x + bullet2w:
     player1_HP -= 10
     bullet2y = 0
     bullet2x = random.randrange(0,300)
     y_BulletChange2 = 0 

#WARUNEK NA ZWYCIESTWO	 
   if player1_WINS == 2:
    gameDisplay.blit(win_image, [0,0])
    winner_1()
   if player2_WINS == 2:
    gameDisplay.blit(win_image, [0,0])
    winner_2()
	
#TWORZENIE SIE KOLEJNYCH RUND	
   if player1_WINS == 0 and player2_WINS == 0:   
    gameDisplay.blit(background_image, [0,0])
    OBSTACLES.obstacles(obs_x, obs_y, obs_width, obs_height)
    OBSTACLES.obstacles2(obs2_x, obs2_y, obs2_width, obs2_height)
    BULLET.bullet_1(bullet1x, bullet1y, bullet1w, bullet1h)
    BULLET.bullet_2(bullet2x, bullet2y, bullet2w, bullet2h)
    CHARACTER.start_Char(x_start_Char,y_start_Char,char_w,char_h)
    CHARACTER.start_Char2(x_start_Char2,y_start_Char2,char_w,char_h)
    SCENE.ground(ground_x, ground_y, ground_width, ground_height)
    round1_text()
    player1HP(player1_HP)
    player2HP(player2_HP)
    player1WINS(player1_WINS)
    player2WINS(player2_WINS)
    player1HPrect(50, 100, 0.5 * player1_HP * 2, 15, red)
    player2HPrect(750, 100, -0.5 * player2_HP * 2, 15, red)
	
   if (player1_WINS == 1 and player2_WINS == 0) or (player1_WINS == 0 and player2_WINS == 1):
    gameDisplay.blit(background_image2, [0,0])
    OBSTACLES.obstacles(obs_x, obs_y, obs_width, obs_height)
    OBSTACLES.obstacles2(obs2_x, obs2_y, obs2_width, obs2_height)
    BULLET.bullet_6(bullet1x, bullet1y, bullet1w, bullet1h)
    BULLET.bullet_4(bullet2x, bullet2y, bullet2w, bullet2h)
    CHARACTER.start_Char3(x_start_Char,y_start_Char,char_w,char_h)
    CHARACTER.start_Char4(x_start_Char2,y_start_Char2,char_w,char_h)
    SCENE.ground2(ground_x, ground_y+27, ground_width, ground_height)
    round2_text()
    player1HP(player1_HP)
    player2HP(player2_HP)
    player1WINS(player1_WINS)
    player2WINS(player2_WINS)
    player1HPrect(50, 100, 0.5 * player1_HP * 2, 15, red)
    player2HPrect(750, 100, -0.5 * player2_HP * 2, 15, red)    
	
   if player1_WINS == 1 and player2_WINS == 1:
    gameDisplay.blit(background_image3, [0,0])
    OBSTACLES.obstacles(obs_x, obs_y, obs_width, obs_height)
    OBSTACLES.obstacles2(obs2_x, obs2_y, obs2_width, obs2_height)
    BULLET.bullet_5(bullet1x, bullet1y, bullet1w, bullet1h)
    BULLET.bullet_3(bullet2x, bullet2y, bullet2w, bullet2h)
    CHARACTER.start_Char5(x_start_Char,y_start_Char,char_w,char_h)
    CHARACTER.start_Char6(x_start_Char2,y_start_Char2,char_w,char_h)
    SCENE.ground3(ground_x, ground_y+15, ground_width, ground_height)
    round3_text()
    player1HP(player1_HP)
    player2HP(player2_HP)
    player1WINS(player1_WINS)
    player2WINS(player2_WINS)
    player1HPrect(50, 100, 0.5 * player1_HP * 2, 15, red)
    player2HPrect(750, 100, -0.5 * player2_HP * 2, 15, red)  

   pygame.display.update()
   clock.tick(29)
  

START.game_menu()
SCENE.game_loop()
pygame.quit()
quit()
