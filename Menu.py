import pygame
from Settings import *
from pygame.locals import *

class START:
 
 def text_objects(text, font):
  textSurface = font.render(text, True, black)
  return textSurface, textSurface.get_rect()
 
 def button(msg, x, y, w, h, inact, act, action = None):
  mouse = pygame.mouse.get_pos()
  click = pygame.mouse.get_pressed()

  if x + w > mouse[0] > x and  y + h > mouse[1] > y:
   pygame.draw.rect(gameDisplay, act, (x, y, w, h))
   if click[0] == 1 and action != None:
    if action == "play":
     SCENE.game_loop()
    elif action == "help":
     HELP.help_screen()
    elif action == "quit":
     pygame.quit()
    elif action == "back":
     INTRO.game_menu()
 
  else:
   pygame.draw.rect(gameDisplay, inact, (x, y, w, h))
   
  smallText = pygame.font.Font("freesansbold.ttf", 32)
  textSurf, textRect = START.text_objects(msg, smallText)  
  textRect.center = ( (x + (w/2)), (y + (h/2)))
  gameDisplay.blit(textSurf, textRect)
 
 def game_menu():#klasa menu
  pygame.mixer.music.stop
  pygame.mixer.music.load('sounds/Menu.mp3')
  pygame.mixer.music.play(-1,0.0)
  
  menu = True
 
  while menu:
   for event in pygame.event.get():
    if event.type == pygame.QUIT:
     quit()
   gameDisplay.blit(menu_image, [0,0])

   START.button("START", 40, 124, 350, 40, button_1Off, button_1, "play")
   START.button("HELP", 40, 180, 350, 40, button_2Off, button_2, "help")
   START.button("EXIT", 40, 236, 350, 40, button_3Off, button_3, "quit")
   pygame.display.update()
   clock.tick(15)